// cpp4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>   

using std::cout;
using std::endl;

int pow(int base, int exp) {
	if (exp == 0) {
		return 1;
	}
	else {
		int result = base;
		for (int i = 0; i < exp - 1; i++) {
			result *= base;
		}
		return result;
	}
	
};

float sine(float opp, float hyp) {
	return opp / hyp;
}

float avg(int arr[], int length) {
	float sum = 0;		
	for (int i = 0; i < length; i++) {		
		sum += arr[i];
	}
	return sum /length;
}

int main()
{
	int power = pow(2, 2);
	float sin = sine(2, 8);
	const int size = 5;
	int arr[size] = { 1,2,3,4,5 };	
	float average = avg(arr, size);
	cout << "Power: " + std::to_string(power) << endl;
	cout << "Sine: " + std::to_string(sin) << endl;
	cout << "Average: " + std::to_string(average) << endl;
	std::cin.get();
    return 0;
}

